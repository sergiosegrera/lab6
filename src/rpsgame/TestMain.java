package rpsgame;

public class TestMain {

	public static void main(String[] args) {
		RpsGame game = new RpsGame();
		for (int i = 0; i < 3; i++) {
			System.out.println(game.playRound("rock"));
		}
		
		System.out.println("wins: " + game.getWins());
		System.out.println("losses: " + game.getLosses());
		System.out.println("ties: " + game.getTies());
	}

}
