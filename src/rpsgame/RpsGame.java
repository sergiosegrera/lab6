// Sergio Segrera 1833693
package rpsgame;

import java.util.Random;


// @author Sergio Segrera
public class RpsGame {
	private int wins;
	private int losses;
	private int ties;
	
	public int getWins() {
		return this.wins;
	}
	
	public int getLosses() {
		return this.losses;
	}
	
	public int getTies() {
		return this.ties;
	}
	
	public String playRound(String move) {
		Random rand = new Random();
		int randomMove = rand.nextInt(3);
		String[] cpuMoves = {"rock", "paper", "scissors"};
		String cpuMove = cpuMoves[randomMove];
		
		if (move.equals(cpuMove)) {
			this.ties++;
			return "Computer plays " + cpuMove + " and ties.";
		}
		
		if (move.equals(cpuMoves[randomMove + 1 == 3 ? 0 : randomMove + 1])) {
			this.wins++;
			return "Computer plays " + cpuMove + " and losses.";
		} else {
			this.losses++;
			return "Computer plays " + cpuMove + " and wins.";
		}
	}
}
