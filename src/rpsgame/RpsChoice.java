// Sergio Segrera 1833693
package rpsgame;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String choice;
	private RpsGame game;
	
	RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String choice, RpsGame game) {
		this.message = message;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.choice = choice;
		this.game = game;
	}
	
	@Override
	public void handle(ActionEvent event) {
		String result = this.game.playRound(this.choice);
		
		message.setText(result);
		
		wins.setText("wins: " + String.valueOf(this.game.getWins()));
		losses.setText("losses: " + String.valueOf(this.game.getLosses()));
		ties.setText("ties: " + String.valueOf(this.game.getTies()));
		
	}

}
