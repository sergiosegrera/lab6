// Sergio Segrera 1833693
package rpsgame;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {
	private RpsGame logic;
	
	public void start(Stage stage) {
		// Init Game logic
		logic = new RpsGame();
		
		Group root = new Group();
		
		// Create Buttons
		Button rockButton = new Button("rock");
		Button paperButton = new Button("paper");
		Button scissorsButton = new Button("scissors");
		HBox buttons = new HBox();
		buttons.getChildren().addAll(rockButton, paperButton, scissorsButton);
		
		// Create TextFields
		TextField message = new TextField("Welcome!");
		message.setPrefWidth(200);
		TextField wins = new TextField("wins");
		TextField losses = new TextField("losses");
		TextField ties = new TextField("ties");
		HBox textFields = new HBox();
		textFields.getChildren().addAll(message, wins, losses, ties);
		
		// Add RpsChoices
		RpsChoice rockChoice = new RpsChoice(message, wins, losses, ties, "rock", logic);
		rockButton.setOnAction(rockChoice);
		RpsChoice paperChoice = new RpsChoice(message, wins, losses, ties, "paper", logic);
		paperButton.setOnAction(paperChoice);
		RpsChoice scissorsChoice = new RpsChoice(message, wins, losses, ties, "scissors", logic);
		scissorsButton.setOnAction(scissorsChoice);
		
		
		// Add Hboxes to VBox
		VBox overall = new VBox();
		overall.getChildren().addAll(buttons, textFields);
		
		root.getChildren().add(overall);
		
		Scene scene = new Scene(root, 650, 300);
		scene.setFill(Color.BLACK);
		
		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);
		
		stage.show();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Application.launch(args);
	}

}
